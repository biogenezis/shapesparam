package com.shapesParam.shapes.admin.cruds;

import com.shapesParam.shapes.admin.modules.Circle;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;

@Component
public interface CircleCRUD extends MongoRepository<Circle, String> {
}
