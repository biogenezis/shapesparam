package com.shapesParam.shapes.admin.cruds;

import com.shapesParam.shapes.admin.modules.Rectangle;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;

@Component
public interface RectangleCRUD extends MongoRepository<Rectangle, String> {
}
