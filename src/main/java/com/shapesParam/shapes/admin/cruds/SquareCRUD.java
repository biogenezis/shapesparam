package com.shapesParam.shapes.admin.cruds;

import com.shapesParam.shapes.admin.modules.Square;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;

@Component
public interface SquareCRUD extends MongoRepository<Square, String> {
}
