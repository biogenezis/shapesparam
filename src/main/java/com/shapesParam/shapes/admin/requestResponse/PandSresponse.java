package com.shapesParam.shapes.admin.requestResponse;

import org.springframework.stereotype.Component;

@Component
public class PandSresponse {
    private double perimeter;
    private double area;

    @Override
    public String toString() {
        return "PandSresponse{" +
                "perimeter=" + perimeter +
                ", area=" + area +
                '}';
    }

    public double getPerimeter() {
        return perimeter;
    }

    public void setPerimeter(double perimeter) {
        this.perimeter = perimeter;
    }

    public double getArea() {
        return area;
    }

    public void setArea(double area) {
        this.area = area;
    }
}
