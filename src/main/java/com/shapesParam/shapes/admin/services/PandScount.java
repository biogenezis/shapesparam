package com.shapesParam.shapes.admin.services;

import com.shapesParam.shapes.admin.cruds.CircleCRUD;
import com.shapesParam.shapes.admin.cruds.RectangleCRUD;
import com.shapesParam.shapes.admin.cruds.SquareCRUD;
import com.shapesParam.shapes.admin.modules.Circle;
import com.shapesParam.shapes.admin.modules.Rectangle;
import com.shapesParam.shapes.admin.modules.Square;
import com.shapesParam.shapes.admin.requestResponse.PandSresponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class PandScount {

@Autowired
private CircleCRUD circleCRUD;
@Autowired
private RectangleCRUD rectangleCRUD;
@Autowired
private SquareCRUD squareCRUD;

@Autowired
private PandSresponse response;

double perimeter;
double area;

public PandSresponse circleCalc(Circle circle) {
     perimeter = circle.getRadius()*2*3.14;
     area = circle.getRadius();
     response.setArea(area);
     response.setPerimeter(perimeter);
     circleCRUD.save(circle);
     return response;
}

public PandSresponse rectangleCalc(Rectangle rectangle) {
    perimeter = (rectangle.getSideA()+rectangle.getSideB())*2;
    area = rectangle.getSideA()*rectangle.getSideB();
    response.setPerimeter(perimeter);
    response.setArea(area);
    rectangleCRUD.save(rectangle);
    return response;
}

public PandSresponse squareCalc(Square square) {
    perimeter = square.getSideA()*4;
    area = square.getSideA()*square.getSideA();
    response.setPerimeter(perimeter);
    response.setArea(area);
    squareCRUD.save(square);
    return response;
}

}
