package com.shapesParam.shapes.admin.modules;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.stereotype.Component;


@Component
@Document(collection = "circles")
public class Circle {
    @Id
    private ObjectId _id;
    private double radius;

    @Override
    public String toString() {
        return "Circle{" +
                "_id=" + _id +
                ", radius=" + radius +
                '}';
    }

    public ObjectId get_id() {
        return _id;
    }

    public void set_id(ObjectId _id) {
        this._id = _id;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }
}
