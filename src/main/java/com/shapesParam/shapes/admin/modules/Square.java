package com.shapesParam.shapes.admin.modules;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.stereotype.Component;

@Component
@Document(collection = "squares")
public class Square {
    @Id
    private ObjectId _id;
    private int sideA;

    @Override
    public String toString() {
        return "Square{" +
                "sideA=" + sideA +
                '}';
    }

    public ObjectId get_id() {
        return _id;
    }

    public void set_id(ObjectId _id) {
        this._id = _id;
    }

    public int getSideA() {
        return sideA;
    }

    public void setSideA(int sideA) {
        this.sideA = sideA;
    }
}
