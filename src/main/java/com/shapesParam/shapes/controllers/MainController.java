package com.shapesParam.shapes.controllers;


import com.shapesParam.shapes.admin.modules.Circle;
import com.shapesParam.shapes.admin.modules.Rectangle;
import com.shapesParam.shapes.admin.modules.Square;
import com.shapesParam.shapes.admin.requestResponse.PandSresponse;
import com.shapesParam.shapes.admin.services.PandScount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/calc-perimeter-area/")
@Controller
public class MainController {

    @Autowired
    PandScount count;

    @GetMapping("/circle/")
    public PandSresponse calcCircle(@RequestBody Circle circle) {
        return count.circleCalc(circle);
    }
    @GetMapping("/rectangle/")
    public PandSresponse calcRectangle(@RequestBody Rectangle rectangle) {
        return count.rectangleCalc(rectangle);
    }

    @GetMapping("/square/")
    public PandSresponse calcSquare(@RequestBody Square square) {
        return count.squareCalc(square);
    }
}
